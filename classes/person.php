<?php
class Person{
    public $full_name = "ФИО";
    public $phone = "Телефон";
    public $email = "email";
    public $ro_le = "Роль";

    public function __construct($full_name, $phone, $email, $ro_le){
        $this->full_name = $full_name;
        $this->phone = $phone;
        $this->email = $email;
        $this->ro_le = $ro_le;
    }

    public function getVisitCard(){
        return $this->full_name . ', ' . $this->phone . ', ' . $this->email . ', ' . $this->ro_le;
    }
}
?>