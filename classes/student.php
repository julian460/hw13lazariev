<?php
class Student extends Person{
    public $averange_mark = "Средняя оценка";

    public function __construct($full_name, $phone, $email, $ro_le, $averange_mark){
        parent::__construct($full_name, $phone, $email, $ro_le);
        $this -> averange_mark = $averange_mark;
    }

    public function getVisitCard(){
        return parent::getVisitCard() . ', ' .  $this->averange_mark;
    }
}
?>