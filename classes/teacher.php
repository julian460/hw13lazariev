<?php
class Teacher extends Person{
    public $sub_ject = "Предмет";

    public function __construct($full_name, $phone, $email, $ro_le, $sub_ject){
        parent::__construct($full_name, $phone, $email, $ro_le);
        $this -> sub_ject = $sub_ject;
    }

    public function getVisitCard(){
        return parent::getVisitCard() . ', ' .  $this->sub_ject;
    }
}
?>