<?php
class Admin extends Person{
    public $working_day = "Рабочие дни";

    public function __construct($full_name, $phone, $email, $ro_le, $working_day){
        parent::__construct($full_name, $phone, $email, $ro_le);
        $this -> working_day = $working_day;
    }

    public function getVisitCard(){
        return parent::getVisitCard() . ', ' .  $this->working_day;
    }
}
?>