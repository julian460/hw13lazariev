<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/database/conect.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/person.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/admin.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/student.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/teacher.php';


try{
    $sql = "SELECT full_name, phone, email, ro_le, working_day, sub_ject, averange_mark  FROM members;";
    $members = $db->query($sql);
    $membersArray = $members->fetchAll();
}catch(Exception $e){
    die('Problem with getting data'. $e->getMessage());
}
$separetedMembers = [];
foreach ($membersArray as $member) {
    if($member['ro_le'] === 'student'){
            $separetedMembers[] = new Student(
                $member['full_name'],
                $member['phone'],
                $member['email'],
                $member['ro_le'],
                $member['averange_mark']);}  
        elseif ($member['ro_le'] === 'admin') {
            $separetedMembers[] = new Admin(
                $member['full_name'],
                $member['phone'],
                $member['email'],
                $member['ro_le'],
                $member['working_day']);}   
        else {
            $separetedMembers[] = new Teacher(
                $member['full_name'],
                $member['phone'],
                $member['email'],
                $member['ro_le'],
                $member['sub_ject']);}             
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Users list">
    <title>Users</title>
</head>
<body>
    <div class="container">
        <h1>List of members</h1>
        <div class="row d-flex justify-content-center">
            <div class="col">
                <ul>
                    <?php foreach($separetedMembers as $member):?>
                        <li>
                        <?=$member->getVisitCard()?>
                        </li>
                    <?php endforeach?>
                </ul>
            </div>
        </div>
    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>