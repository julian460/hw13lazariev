<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/database/conect.php';
$seedMembers = [
    ['full_name' => 'John Doe','phone' => '123-11-11', 'email' => 'john@examp.com', 'ro_le' => 'admin', 'working_day' => 'Monday, Wednesday, Friday'],
    ['full_name' => 'Bob Robinson','phone' => '321-11-11',  'email' => 'robinson@examp.com', 'ro_le' => 'admin', 'working_day' => 'Tuesday, Thursday, Saturday'],
    ['full_name' => 'Jack Smith','phone' => '123-22-11',  'email' => 'smith@examp.com', 'ro_le' => 'teacher', 'sub_ject' => 'PHP'],
    ['full_name' => 'Anna White','phone' => '123-11-99',  'email' => 'anna@examp.com', 'ro_le' => 'teacher', 'sub_ject' => 'Javascripte'],
    ['full_name' => 'Adam Hawk','phone' => '258-11-85',  'email' => 'adam@examp.com', 'ro_le' => 'teacher', 'sub_ject' => 'React'],
    ['full_name' => 'Nick Jefferson','phone' => '627-53-11',  'email' => 'nick@examp.com', 'ro_le' => 'student', 'averange_mark' => 69.7],
    ['full_name' => 'Chris Jefferson','phone' => '632-43-11',  'email' => 'jefferson@examp.com', 'ro_le' => 'student', 'averange_mark' => 93.7],
    ['full_name' => 'Liza Baker','phone' => '963-45-32',  'email' => 'baker@examp.com', 'ro_le' => 'student', 'averange_mark' => 99.1],
    ['full_name' => 'Ryan Gosling','phone' => '157-91-11',  'email' => 'ryan@examp.com', 'ro_le' => 'student', 'averange_mark' => 55.1],
    ['full_name' => 'Elza Woodstock','phone' => '639-75-11',  'email' => 'woodstock@examp.com', 'ro_le' => 'student', 'averange_mark' => 86.9],
];
foreach($seedMembers as $member){
    switch($member['ro_le']){
        case 'student':
            try{
                $sql = "INSERT INTO members SET 
                full_name='" . $member['full_name'] . "',
                phone='" . $member['phone'] . "',
                email='" . $member['email'] . "',
                ro_le='" . $member['ro_le'] . "',
                averange_mark= {$member['averange_mark']};";
                $db->exec($sql);
            }catch(Exception $error){
                die('Error inserting test data'. $error->getMessage());
        };
        break;
        case 'teacher' :
            try{
                $sql = "INSERT INTO members SET 
                full_name='" . $member['full_name'] . "',
                phone='".$member['phone']."',
                email='".$member['email']."',
                ro_le='".$member['ro_le']."',
                sub_ject='".$member['sub_ject']."'";
                $db->exec($sql);
            }catch(Exception $error){
                die('Error inserting test data'. $error->getMessage());
        };
        break; 
        case 'admin' :
            try{
                $sql = "INSERT INTO members SET 
                full_name='" . $member['full_name'] . "',
                phone='".$member['phone']."',
                email='".$member['email']."',
                ro_le='".$member['ro_le']."',
                working_day='".$member['working_day']."'";
                $db->exec($sql);
            }catch(Exception $error){
                die('Error inserting test data'. $error->getMessage());
        };
        break;

    }
}
echo 'data added successfully';